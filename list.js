
const blessed = require('blessed')

let screen = blessed.screen({
  title: 'todo',
  debug: true,
  log: './debug.log'
})

let i = [
  'aaa',
  'bbbb',
  'ccccc',
  'dddddd',
  'eeeeeee'
]

screen.debug('length', i.length)

let list = blessed.List({
  top: 0,
  left: 0,
  width: '100%',
  height: 20,
  border: {type: 'line'},
  style: {
    border: {
      fg: 'white'
    },
    item: {
      fg: 'white'
    },
    selected: {
      fg: 'auto',
      bg: 'grey'
    }
  },
  items: i
})

screen.append(list)

let index = 0

screen.on('keypress', function (ch, key) {
  if (key.name === 'q') {
    process.exit(0)
  }

  if (key.full === 'up') {
    index = Math.max(0, index - 1)
    list.select(index)
  }

  if (key.full === 'down') {
    index = Math.min(i.length, index + 1)
    screen.debug(index, ':', i.length, i)
    list.select(index)
  }

  // screen.render()
})

screen.render()
