
exports.createDispatch = function (action, payload) {
  return {action, payload}
}

exports.clamp = function (val, min, max) {
  return val < min ? min : val > max ? max : val
}
