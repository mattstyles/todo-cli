
const h = require('react-hyperscript')
const {render} = require('react-blessed')
const screen = require('../screen')
const {STATES} = require('../const')

const Todos = require('./todos')
const Prompt = require('./prompt')
const Filter = require('./filter')

let App = props => {
  return (
    h('element', [
      h(Prompt, {
        focussed: props.model.state === STATES.EDITING
      }),
      h(Todos, {
        items: props.model.todos,
        index: props.model.index,
        focussed: props.model.state === STATES.SELECTING
      }),
      h(Filter, {
        focussed: props.model.state === STATES.FILTERING
      })
    ])
  )
}

module.exports = function view (model) {
  return function update (previous, current) {
    render(h(App, {model: current}), screen)
  }
}
