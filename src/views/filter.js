
const h = require('react-hyperscript')

const Todos = props => {
  return (
    h('list', {
      top: 0,
      right: 0,
      width: 20,
      height: 3,
      scrollable: true,
      border: {type: 'line'},
      style: {
        border: {
          fg: props.focussed ? 'white' : 'grey'
        }
      },
      items: props.items
    })
  )
}

Todos.defaultProps = {
  items: []
}

module.exports = Todos
