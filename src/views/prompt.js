
const h = require('react-hyperscript')

const Prompt = props => {
  return (
    h('list', {
      top: 0,
      left: 0,
      right: 20,
      height: 3,
      scrollable: true,
      border: {type: 'line'},
      style: {
        border: {
          fg: props.focussed ? 'white' : 'grey'
        }
      },
      items: ['a1', 'b1', 'c']
    })
  )
}

module.exports = Prompt
