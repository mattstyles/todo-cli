
const {Component} = require('react')
const h = require('react-hyperscript')
const screen = require('../screen')

class Todos extends Component {
  componentDidMount () {
    this.refs.list.select(this.props.index)
  }

  render () {
    let {items, focussed} = this.props
    return h('list', {
      ref: 'list',
      top: 3,
      left: 0,
      bottom: 0,
      width: '100%',
      scrollable: true,
      border: {type: 'line'},
      style: {
        border: {
          fg: focussed ? 'white' : 'grey'
        },
        item: {
          fg: 'auto',
          bg: 'black'
        },
        selected: {
          fg: 'auto',
          bg: focussed ? 'grey' : 'black'
        }
      },
      items: items
    })
  }
}

Todos.defaultProps = {
  items: []
}

module.exports = Todos
