
const actionkey = require('actionkey')

const APP = actionkey('APP', [
  'QUIT',
  'SHIFT_FOCUS'
])

const TODOS = actionkey('TODO', [
  'ADD',
  'DELETE',
  'NAVIGATE'
])

const STATES = actionkey('STATES', [
  'EDITING',
  'FILTERING',
  'SELECTING'
])

module.exports = {
  APP,
  TODOS,
  STATES
}
