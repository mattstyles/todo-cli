
const screen = require('./screen')
const {app} = require('./models')
const intent = require('./intents')
const update = require('./updates')
const view = require('./views/app')

function woah (err) {
  screen.debug('error', err)
  setTimeout(() => {
    process.exit(1)
  }, 100)
}

process.on('error', woah)
process.on('uncaughtException', woah)
process.on('unhandledRejection', woah)

function run (model, update, intent, sink) {
  let previous = model
  let view = sink(previous)
  intent()
    .scan(update, previous)
    .observe(current => {
      screen.debug(current)
      view(previous, current)
      previous = current
    }, woah)
}

run(app, update, intent, view)
