
const screen = require('./screen')
const {APP, STATES, TODOS} = require('./const')
const rotate = require('rotate-array')
const {clamp} = require('./util')

let mutStates = Object.keys(STATES)
  .map(key => STATES[key])

function updateViewing (model, dispatch) {
  if (dispatch.action === TODOS.NAVIGATE) {
    return model.set('index', clamp(
      model.index + dispatch.payload,
      0,
      model.get('todos').size
    ))
  }

  return model
}

module.exports = function update (model, dispatch) {
  if (dispatch.action === APP.QUIT) {
    screen.debug('quitting...')
    process.exit(0)
  }

  if (dispatch.action === APP.SHIFT_FOCUS) {
    mutStates = rotate(mutStates, dispatch.payload)
    return model.set('state', mutStates[0])
  }

  if (model.state === STATES.SELECTING) {
    return updateViewing(model, dispatch)
  }

  return model
}
