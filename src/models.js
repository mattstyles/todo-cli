
const Immutable = require('immutable')
const {STATES} = require('./const')

const AppModel = new Immutable.Record({
  field: '',
  state: STATES.EDITING,
  index: 0,
  todos: new Immutable.List()
})

exports.app = new AppModel({
  todos: new Immutable.List([
    'do stuff',
    'test more stuff',
    'test less stuff',
    'do stuff',
    'test more stuff',
    'test less stuff',
    'do stuff',
    'test more stuff',
    'test less stuff',
    'do stuff',
    'test more stuff',
    'test less stuff',
    'do stuff',
    'test more stuff',
    'test less stuff',
    'do stuff',
    'test more stuff',
    'test less stuff',
    'do stuff',
    'test more stuff',
    'test less stuff',
    'do stuff',
    'test more stuff',
    'test less stuff',
    'do stuff',
    'test more stuff',
    'test less stuff',
    'do stuff',
    'test more stuff',
    'test less stuff',
    'do stuff',
    'test more stuff',
    'test less stuff',
    'do stuff',
    'test more stuff',
    'test less stuff'
  ])
})
