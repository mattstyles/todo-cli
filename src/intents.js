
const most = require('most')
const {createDispatch} = require('./util')
const {APP, TODOS} = require('./const')
const {keys} = require('./signals')

const screen = require('./screen')

function getFullKey (key) {
  return key.key.full
}

function quitStream (signal) {
  return signal
    .filter(key => ['C-c', 'escape'].includes(key))
    .map(key => createDispatch(APP.QUIT))
}

function focusStream (signal) {
  return most.merge(
    signal
      .filter(key => key === 'S-tab')
      .map(key => createDispatch(APP.SHIFT_FOCUS, -1)),
    signal
      .filter(key => key === 'tab')
      .map(key => createDispatch(APP.SHIFT_FOCUS, 1))
  )
}

function navigateStream (signal) {
  return most.merge(
    signal
      .filter(key => ['up', 'left'].includes(key))
      .map(key => createDispatch(TODOS.NAVIGATE, -1)),
    signal
      .filter(key => ['down', 'right'].includes(key))
      .map(key => createDispatch(TODOS.NAVIGATE, 1))
  )
}

module.exports = function intent () {
  return most.merge(
    quitStream(keys.map(getFullKey)),
    // focusStream(keys.filter(key => key.key.full === 'tab')),
    focusStream(keys.map(getFullKey)),
    navigateStream(keys.map(getFullKey))
  )
}
